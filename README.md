# UserNotFound

UserNotFound is a 2-player online physics puzzle game.

Using unique physics and lasers, work with the other player to solve puzzles and reconnect a broken and glitchy network.

This game was made as a collaboration between students at USC Games and Berklee College of Music.

Note: This project will not run when opened in the Unity Editor as its online matchmaking server keys have been removed.

[Play the Game](https://td01.itch.io/usernotfound)
