using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour
{

    /*
        Handles changing of game options.
    */

    public void updateSensitivity(float sensitivity) {
        Globals.mouseSensitivity = sensitivity;
    }

    public void updateBackground(float background) {
        Globals.backgroundVolume = background;
    }

    public void updateEffects(float volume) {
        Globals.effectsVolume = volume;
    }
}
