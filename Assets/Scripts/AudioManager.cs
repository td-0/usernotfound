using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    /*
        Controls Music and SFX -- including transitions between tracks
    */

    public Sound[] sounds;

    private bool[] fading;

    void Awake()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;

        }

        fading = new bool[sounds.Length];
    }
    // background music
    void Start()
    {
        StartCoroutine(FadeIn("menu ambiance", 0.01f, Globals.backgroundVolume));
        StartCoroutine(FadeIn("menu beat", 0.01f, Globals.backgroundVolume));
    }

    // Change Volumes
    void Update()
    {
        // Background Music
        
        for (int iter = 4; iter < 11; iter++) {
            if (sounds[iter].source.volume != Globals.backgroundVolume && !fading[iter]) sounds[iter].source.volume = Globals.backgroundVolume;
        }

        foreach (Sound s in sounds)
        {
            if(s == sounds[4] || s == sounds[5] || s == sounds[6] || s == sounds[7] || s == sounds[8] || s == sounds[9] || s == sounds[10]
            || s == sounds[16] || s == sounds[17] || s == sounds[18]) continue;
            if (s != sounds[4] && s.source.volume != Globals.effectsVolume) s.source.volume = Globals.effectsVolume;
        }

        // Change Level 2 initial to loop
        if(sounds[5].source.time > 106 && !sounds[7].source.isPlaying) sounds[7].source.Play();
        if(sounds[6].source.time > 106 && !sounds[8].source.isPlaying) sounds[8].source.Play();

    }

    public void Transition(string prev, string curr, float rateModifier) {
        float fadeRate = (Globals.backgroundVolume / 1000) * rateModifier;
        if (prev != "") StartCoroutine(FadeOut(prev, fadeRate));
        if (curr != "") StartCoroutine(FadeIn(curr, fadeRate, Globals.backgroundVolume));
    }

    // Play sound method
    public void Play(string name)
    {   
        // find a right sound source from the array. using System;
        Sound s = Array.Find(sounds, sound => sound.name == name);
        
        if (s == null)
        {
            Debug.LogWarning("Sound"+name+" not found!");
            return;
        }

        s.source.Play();
    }

    public void Stop(string name)
    {   
        // find a right sound source from the array. using System;
        Sound s = Array.Find(sounds, sound => sound.name == name);
        
        if (s == null)
        {
            Debug.LogWarning("Sound"+name+" not found!");
            return;
        }

        s.source.Stop();
    }

    IEnumerator FadeIn(string name, float rate, float targetVolume) {
        int soundIndex = Array.FindIndex(sounds, sound => sound.name == name);
        fading[soundIndex] = true;

        Sound s = sounds[soundIndex];
        s.source.volume = 0f;
        s.source.Play();

        while(s.source.volume < targetVolume) {
            s.source.volume += rate;
            yield return null;
        }

        fading[soundIndex] = false;
    }

    IEnumerator FadeOut(string name, float rate) {
        int soundIndex = Array.FindIndex(sounds, sound => sound.name == name);
        fading[soundIndex] = true;

        Sound s = sounds[soundIndex];
        while(s.source.volume > 0f) {
            s.source.volume -= rate;
            yield return null;
        }

        s.source.Stop();
        fading[soundIndex] = false;

    }
}
