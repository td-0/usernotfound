using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleMenu : MonoBehaviour
{
    [SerializeField] GameObject[] menus;
    
    public void ToggleSubMenu(int menuIndex)
    {
        bool state = menus[menuIndex].activeSelf;
        if (!state)
        {
            foreach (GameObject menu in menus)
            {
                menu.SetActive(false);
            }
        }
        menus[menuIndex].SetActive(!state);
    }
}
