using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnimation : MonoBehaviour
{
    public void Rotate180()
    {
        iTween.RotateBy(this.gameObject, iTween.Hash("amount", new Vector3(0,0,-1.0f),"time", 0.5f, "easetype",iTween.EaseType.easeInOutCubic));
    }
}
