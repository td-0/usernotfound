using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHover : MonoBehaviour
{
    private float BGfadeAlpha = 0.196f;
    private float BTNfadeAlpha = 0.7f;

    public void hovering() {
        Image image = GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, BGfadeAlpha);
    }

    public void notHovering() {
        Image image = GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0.0f);
    }

    public void BTNhovering()
    {
        Image image = GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, BTNfadeAlpha);
    }

    public void BTNnotHovering()
    {
        Image image = GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, 1.0f);
    }
}
