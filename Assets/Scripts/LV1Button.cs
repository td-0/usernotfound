using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class LV1Button : MonoBehaviour
{
    // OBJ MATERIALS
    private Renderer buttonRenderer;
    private Material[] buttonMaterials;
    
    // ASSIGNABLE PLAYER MATEIRALS
    private Material[] PlayerMaterials;
    
    // ATTRIBUTE / MODE
    [FormerlySerializedAs("targetPlayerNum")] [FormerlySerializedAs("player")] 
    [SerializeField] public PlayerItem.Player targetPlayer;

    // CONNECTION STATUS
    public bool status;
    private Collider currHit;
    
    // CONNECTED PATH MATERIALS
    [SerializeField] private GameObject pathParent;
    Renderer[] paths;

    // CONNECTED BUTTON & TARGET
    [SerializeField] private GameObject connectedButton;
    private LV1Button connectedButtonScript;
    [SerializeField] private GameObject connectedTarget;
    private Target connectedTargetScript;
    
    // SFXs
    private AudioManager _audioManager;
    
    private void Start()
    {
        // get object materials
        buttonRenderer = transform.GetChild(0).gameObject.GetComponent<Renderer>();
        buttonMaterials = buttonRenderer.materials;
        // get player assignable materials
        PlayerMaterials = GameObject.Find("PlayerColor").GetComponent<MeshRenderer>().materials;
        // get path materials
        if (pathParent != null) paths = pathParent.GetComponentsInChildren<Renderer>();
        // get connected button & targets
        if (connectedButton != null) connectedButtonScript = connectedButton.GetComponent<LV1Button>();
        if (connectedTarget != null) connectedTargetScript = connectedTarget.GetComponent<Target>();
        // get audio manager
        _audioManager = FindObjectOfType<AudioManager>();
        // set initial conditions
        status = false;
    }


    private void OnTriggerEnter(Collider other)
    {
        // if it collides with non interactive object
        if (other != null 
            && other.gameObject.GetComponent<PlayerItem>() != null 
            && other.transform.CompareTag("PickupItem"))
        {
            currHit = other;
            UpdateButton();
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("PickupItem"))
        {
            currHit = null;
            UpdateButton();
        }
    }

    // Condition: When the PickUpItem hits the button
    public void UpdateButton()
    {
        PlayerItem.Player otherTargetPlayer;
        
        // handle case when there's no colliding box
        if (currHit == null)otherTargetPlayer = PlayerItem.Player.NA;
        else otherTargetPlayer = currHit.gameObject.GetComponent<PlayerItem>().player;
        
        // 1. update the connection status
        UpdateConnectionStatus(otherTargetPlayer); 
        // 2. update button material based on status
        UpdateButtonMaterial();
        // 3. update connected button & target based on status
        UpdateConnectedObjects();
    }

    

    private void UpdateConnectionStatus(PlayerItem.Player otherTargetPlayer)
    {
        // 0. BUTTON IS DISABLED
        if (targetPlayer == PlayerItem.Player.NA)
        {
            status = false;
            return;
        }
        // 1. CONNECTED CORRECTLY
        if (targetPlayer == otherTargetPlayer)
        {
            status = true;
            // SFX
            FindObjectOfType<AudioManager>().Play("activate");
            // GLITCH FX
            GameObject.FindWithTag("MainCamera").GetComponent<EditValues>().GlitchForSeconds(0.2f, 0.3f);
        }
        // 2. CONNECTED INCORRECTLY
        else 
        {
            status = false;
        }
    }

    // update button color and the connected path color
    public void UpdateButtonMaterial()
    {
        // 1. CONNECTED CORRECTLY
        if (status)
        {
            buttonMaterials[2] = PlayerMaterials[(int)targetPlayer];
            buttonRenderer.materials = buttonMaterials;
            if (pathParent != null) foreach (Renderer path in paths) path.material = PlayerMaterials[(int)targetPlayer];
        }
        // 2. CONNECTED INCORRECTLY
        else
        {
            // reset color to disabled
            buttonMaterials[2] = PlayerMaterials[(int)PlayerItem.Player.NA];
            buttonRenderer.materials = buttonMaterials;
            if (pathParent != null)
            {
                if (pathParent != null) foreach (Renderer path in paths) path.material = PlayerMaterials[(int)PlayerItem.Player.NA];
            }
        }
    }

    // Update connected objects' mode and call their update function
    private void UpdateConnectedObjects()
    {
        // 1. CONNECTED CORRECTLY
        if (status)
        {
            if (connectedButtonScript != null) connectedButtonScript.SetButtonMode(targetPlayer);
            else if (connectedTargetScript != null) connectedTargetScript.SetTargetConnection(targetPlayer);
        }
        // 2. CONNECTED INCORRECTLY
        else
        {
            if (connectedButtonScript != null) connectedButtonScript.SetButtonMode(PlayerItem.Player.NA);
            else if (connectedTargetScript != null) connectedTargetScript.SetTargetConnection(PlayerItem.Player.NA);
        }
    }
    
    // set the button mode
    public void SetButtonMode(PlayerItem.Player player)
    {
        if (targetPlayer == PlayerItem.Player.NA && player == PlayerItem.Player.NA)
        {
            return;
        }
        targetPlayer = player;
        
        buttonMaterials[1] = PlayerMaterials[(int)player];
        buttonRenderer.materials = buttonMaterials;
        Debug.Log("Set"+this.gameObject.name+" = "+(int)player);
        UpdateButton();
    }
    
}
