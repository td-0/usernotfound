using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementLocalOnly : MonoBehaviour
{

    /*
        Controls directional movement and jump for non-networked player
    */

    // Player and Camera Movement

    [SerializeField] private float speed;

    [SerializeField] private int playerNum;

    CharacterController characterController;

    private Camera cam;

    private Vector3 moveDirection = Vector3.zero;

    private Collider col;

    private bool jumping;

    private float gravity;

    private ESCbehavior pause;

    // Mouse Movement
    private float  mouseSensitivity;

    private float XMouse;

    private float YMouse;

    // Start is called before the first frame update
    void Start()
    {
        mouseSensitivity = Globals.mouseSensitivity;

        characterController = GetComponent<CharacterController>();
        col = GetComponent<Collider>();
        cam = Camera.main;
        jumping = false;

        gravity = 3.0f;

        pause = GameObject.FindWithTag("Esc").GetComponent<ESCbehavior>();
    }

    // Update is called once per frame
    void Update()
    {

        mouseSensitivity = Globals.mouseSensitivity;

        if (Globals.activePlayer == playerNum) {

            // Make Camera Follow Mouse
            XMouse += Input.GetAxis("Mouse X") * mouseSensitivity;
            YMouse -= Input.GetAxis("Mouse Y") * mouseSensitivity;

            YMouse = Mathf.Clamp(YMouse, -90f, 90f);

            if (!pause.isPaused()) {
                if (playerNum == 1) {
                    cam.transform.localEulerAngles = new Vector3(YMouse, XMouse, 0);
                    this.transform.localEulerAngles = new Vector3(0, XMouse, 0);
                }
                else {
                    cam.transform.localEulerAngles = new Vector3(YMouse * -1, XMouse * -1, 180);
                    this.transform.localEulerAngles = new Vector3(0, XMouse * -1, 180);
                }
            
                
                // Change Camera and Gravity, take in directional input
                if (Input.GetButtonDown("CameraChangeP1")) Globals.activePlayer = 1;

                cam.transform.position = this.transform.position;

                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            
                moveDirection *= speed;

                moveDirection = transform.TransformDirection(moveDirection);

                if (!jumping) moveDirection.y = gravity;

                if (Input.GetButtonDown("Jump") && isGrounded()){
                    jumping = true;
                    StartCoroutine(jump(transform.position.y));
                }

                characterController.Move(moveDirection * Time.deltaTime);
            }
        }
    }

    // Jump Function
    private IEnumerator jump(float preJumpY)
    {
        float goalPoint;

        // Stop jump if player cannot go up
        int stuckTime = 0;
        float prevY = transform.position.y;

        goalPoint = preJumpY - 1.7f;
        while (transform.position.y > goalPoint + 0.1f) {
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, goalPoint, 0.2f), transform.position.z);

            if (Mathf.Round(transform.position.y) == Mathf.Round(prevY)) stuckTime++;
            if (stuckTime > 20) break;

            yield return null;
        }
        jumping = false;

        
    }

    //checks if player is on the ground
    private bool isGrounded() {
        RaycastHit hit;

        if (playerNum == 1 && Physics.Raycast(transform.position, Vector3.down, out hit)) {

            Transform selection = hit.transform;

            if (selection.CompareTag("PickupItem") || hit.distance >= col.bounds.extents.y + 0.5f) return false;
            else return true;

        }
        else if (playerNum == 2 && Physics.Raycast(transform.position, Vector3.up, out hit)) {

            Transform selection = hit.transform;

            if (selection.CompareTag("PickupItem") || hit.distance >= col.bounds.extents.y + 0.5f) return false;
            else return true;
        }
        else {
            return false;
        }
    }

}
