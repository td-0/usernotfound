using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{

    /*
        Simple script to prevent object from being
        destroyed when scene is reloaded. Used to
        prevent EOSSDK object from resetting as
        this breaks the network system.
    */

    void Awake() {
        DontDestroyOnLoad(gameObject);
    }
}
