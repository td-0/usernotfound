﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Mirror;

public class CustomNetworkManager : NetworkManager
{

    /*  
        Inherits from standard Mirror Network Manager
        Overrides certain functions to provide custom spawning and connection/disconnection
        Edited EOSLobbyUI script -- for custom Lobby and Lobby UI functionality -- 
        remains in original directory: Assets/Mirror/Runtime/Transport/EpicOnlineTransport/Lobby
    */

    public GameObject player1;
    public GameObject player2;

    [SerializeField] private GameObject nonNetworkP2;

    private NetworkConnection hostConn;
    private bool hostConnected = false;

    private NetworkConnection clientConn;

    [Space(10)]
    [SerializeField] private GameObject createGameUI;
    [SerializeField] private Button start;
    
    // Spawn position
    
    // Tutorial
    private Vector3 player1SpawnPos = new Vector3(-1, -4, -60);
    private Vector3 player2SpawnPos = new Vector3(-6, 24, -60);

    public override void OnClientConnect()
    {
        if (!NetworkClient.ready) NetworkClient.Ready();

        Cursor.lockState = CursorLockMode.Locked;
        NetworkClient.AddPlayer();
    }

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        // Single Player Mode
        if (!createGameUI.activeSelf) {
            Globals.singlePlayer = true;

            GameObject newPlayerOne = Instantiate(player1);
            newPlayerOne.transform.position = player1SpawnPos;
            NetworkServer.AddPlayerForConnection(conn, newPlayerOne);
            Cursor.lockState = CursorLockMode.Locked;
        }

        else {

            if (!hostConnected) {
                hostConn = conn;
                hostConnected = true;
            }
            else {
                clientConn = conn;
                start.interactable = true;
            }

            Cursor.lockState = CursorLockMode.None;
        }

    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        Cursor.lockState = CursorLockMode.None;
        GetComponent<EOSLobbyUI>().LeaveLobby();
    }

    public override void OnClientDisconnect()
    {
        Cursor.lockState = CursorLockMode.None;
        GetComponent<EOSLobbyUI>().LeaveLobby();
    }

    
    public void startGame()
    {
        spawnPlayers();
    }


    private void spawnPlayers() {
        nonNetworkP2.SetActive(false);

        GameObject newPlayerOne = Instantiate(player1);
        GameObject newPlayerTwo = Instantiate(player2);
        newPlayerOne.transform.position = player1SpawnPos;
        newPlayerTwo.transform.position = player2SpawnPos;
        NetworkServer.AddPlayerForConnection(hostConn, newPlayerOne);
        NetworkServer.AddPlayerForConnection(clientConn, newPlayerTwo);
        Cursor.lockState = CursorLockMode.Locked;
    }

}
