using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;
using Mirror;
using UnityEngine.Networking.Match;
using UnityEngine.Serialization;

public class Target : NetworkBehaviour
{

    /*
        Controls behavior of targets hit by laser
        Changes their color, updates status, and
        sends updated information over network
    */
    
    // OBJ MATERIALS
    private Renderer targetRenderer;
    
    // ASSIGNABLE PLAYER MATEIRALS
    private Material[] PlayerMaterials;

    // ATTRIBUTE / MODE
    [FormerlySerializedAs("targetPlayerNum")]
    [FormerlySerializedAs("targetConnection")] 
    [SerializeField] private PlayerItem.Player targetPlayer;

    // CONNECTION STATUS
    private bool status;
    
    private int lastHit; // -1 not hit
    private float FADE_TIME = 3.0f;
    private float fadeTimer;
    public bool isFading;
    
    // CONNECTED PATH MATERIALS
    [SerializeField] private GameObject pathParent;
    private Renderer[] paths;
    
    // CONNECTED BUTTON & TARGET
    [SerializeField] private GameObject connectedButton;
    private LV1Button connectedButtonScript;
    
    [SerializeField] private GameObject connectedTarget;
    private Target connectedTargetScript;
    
    // SFXs
    private AudioManager _audioManager;
    
    // FADE EFFECT 
    private IEnumerator fadeEffectCoroutine;
    private void Start()
    {
        // get object materials
        targetRenderer = this.gameObject.GetComponent<MeshRenderer>();
        // get player assignable materials
        PlayerMaterials = GameObject.Find("PlayerColor").GetComponent<MeshRenderer>().materials;
        // get path materials
        paths = pathParent.GetComponentsInChildren<Renderer>();
        // get connected button & targets
        if (connectedButton != null) connectedButtonScript = connectedButton.GetComponent<LV1Button>();
        if (connectedTarget != null) connectedTargetScript = connectedTarget.GetComponent<Target>();
        // get audio manager
        _audioManager = FindObjectOfType<AudioManager>();
        // set coroutine
        fadeEffectCoroutine = null;
        // set initial conditions
        lastHit = -1;
        status = false;
    }

    public void hit(int playerNum)
    {
        if (lastHit == playerNum) return;
        
        UpdateTarget((PlayerItem.Player) playerNum);
        
        lastHit = playerNum;
    }

    // Condition: When the raycast hit the target
    public void UpdateTarget(PlayerItem.Player otherTargetPlayer)
    {
        // 1. update the connection status
        UpdateConnectionStatus(otherTargetPlayer);
        // 2. update button material based on status
        UpdateTargetMaterial(otherTargetPlayer);
        // 3. update connected button & target based on status
        UpdateConnectedObjects();

        
        if (isServer)
        {
            RpcSwitchColor(otherTargetPlayer);
        }
        else
        {
            CmdSwitchColor(otherTargetPlayer);
        }
    }

    void UpdateConnectionStatus(PlayerItem.Player otherPlayerTarget)
    {
        // 1. connect -> input not 2 target not 2 they are same
        // 2. reset -> input 2 
        // 3. fade -> input not 2 but not the same
        
        // 0. TARGET IS DISABLED
        if (targetPlayer == PlayerItem.Player.NA)
        {
            status = false;
            lastHit = -1;
            return;
        }
        // 1. CONNECTED CORRECTLY
        if (targetPlayer == otherPlayerTarget)
        {
            status = true;
            // SFX
            _audioManager.Play("raycast_hit");
            // GLITCH FX
            GameObject.FindWithTag("MainCamera").GetComponent<EditValues>().GlitchForSeconds(0.1f, 0.1f);
        }
        // 2. CONNECTED INCORRECTLY
        else
        {
            status = false;
            lastHit = -1;
        }
    }
    void UpdateTargetMaterial(PlayerItem.Player otherPlayerTarget)
    {
        if (fadeEffectCoroutine != null) StopCoroutine(fadeEffectCoroutine);
        // 0. TARGET IS DISABLED
        if (targetPlayer == PlayerItem.Player.NA || otherPlayerTarget == PlayerItem.Player.NA)
        {
            targetRenderer.material = PlayerMaterials[(int)PlayerItem.Player.NA];
            if (pathParent != null) foreach (Renderer path in paths) path.material = PlayerMaterials[(int)PlayerItem.Player.NA];
            return;
        }
        // 1. CONNECTED CORRECTLY
        if (status)
        {
            targetRenderer.material = PlayerMaterials[(int)otherPlayerTarget];
            if (pathParent != null) foreach (Renderer path in paths) path.material = PlayerMaterials[(int)targetPlayer];
        }
        // 2. CONNECTED INCORRECTLY
        else
        {
            // fading coroutine; 
            fadeEffectCoroutine = FadeReset((int) otherPlayerTarget);
            StartCoroutine(fadeEffectCoroutine);
            if (pathParent != null) foreach (Renderer path in paths) path.material = PlayerMaterials[(int)PlayerItem.Player.NA];
        }
    }

    IEnumerator FadeReset(int playerNum)
    {
        targetRenderer.material = PlayerMaterials[playerNum];
        yield return new WaitForSeconds(2);
        targetRenderer.material = PlayerMaterials[(int)PlayerItem.Player.NA];
    }

    // Update connected objects' mode and call their update function
    private void UpdateConnectedObjects()
    {
        // 1. CONNECTED CORRECTLY
        if (status)
        {
            if (connectedButtonScript != null) connectedButtonScript.SetButtonMode(targetPlayer);
            else if (connectedTargetScript != null) connectedTargetScript.SetTargetConnection(targetPlayer);
        }
        // 2. CONNECTED INCORRECTLY
        else
        {
            if (connectedButtonScript != null) connectedButtonScript.SetButtonMode(PlayerItem.Player.NA);
            else if (connectedTargetScript != null) connectedTargetScript.SetTargetConnection(PlayerItem.Player.NA);
        }
    }

    // Update Color on Clients
    [ClientRpc]
    void RpcSwitchColor(PlayerItem.Player otherTargetPlayer)
    {
        // 1. update the connection status
        UpdateConnectionStatus(otherTargetPlayer);
        // 2. update button material based on status
        UpdateTargetMaterial(otherTargetPlayer);
        // 3. update connected button & target based on status
        UpdateConnectedObjects();
    }
    
    // Update Color on Server
    [Command(requiresAuthority = false)]
    void CmdSwitchColor(PlayerItem.Player otherTargetPlayer)
    {
        // 1. update the connection status
        UpdateConnectionStatus(otherTargetPlayer);
        // 2. update button material based on status
        UpdateTargetMaterial(otherTargetPlayer);
        // 3. update connected button & target based on status
        UpdateConnectedObjects();
    }

    public void SetTargetConnection(PlayerItem.Player player)
    {
        if (targetPlayer == PlayerItem.Player.NA && player == PlayerItem.Player.NA)
        {
            return;
        }
        targetPlayer = player;
        Debug.Log("Set"+this.gameObject.name+" = "+(int)player);
        UpdateTarget(PlayerItem.Player.NA);
    }
}
