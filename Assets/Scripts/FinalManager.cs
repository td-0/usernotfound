using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalManager : MonoBehaviour
{
    [SerializeField] private FinalDestination P1dest;
    [SerializeField] private FinalDestination P2dest;
    private bool P1state;
    private bool P2state;
    [SerializeField] private GameObject endMenu;
    private bool gameClear;
    [SerializeField] private EditValues glitchControl;
    
    void Start()
    {
        P1state = P1dest.state;
        P2state = P2dest.state;
        gameClear = P1state && P2state;
    }

    // Update is called once per frame
    void Update()
    {
        P1state = P1dest.state;
        P2state = P2dest.state;
        
        if (P1state && P2state && !gameClear)
        {
            Debug.Log("final clear");
            GameClear();
        }

        gameClear = P1state && P2state;
    }

    void GameClear()
    {
        RunGlitch();
    }
    public void RunGlitch()
    {
        StartCoroutine(DoGlitch());
    }

    IEnumerator DoGlitch()
    {
        glitchControl.SetGlitch(3.0f);
        yield return new WaitForSeconds(1.0f);
        glitchControl.StopGlitch();
        StartCoroutine(FadeScreenOut());
    }

    IEnumerator FadeScreenOut() {

        endMenu.SetActive(true);

        Image menuBackground = endMenu.transform.GetChild(0).GetComponent<Image>();


        while (menuBackground.color.a < 0.99f) {
            menuBackground.color = Color.Lerp(menuBackground.color, Color.black, Time.deltaTime);
            yield return null;
        }

        GameObject.FindWithTag("Crosshair").SetActive(false);
        endMenu.transform.GetChild(1).gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }
    
}
