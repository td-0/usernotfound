using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItem : MonoBehaviour
{
    public enum Player
    {
        P1=0,
        P2,
        NA
    }
    public enum Level
    {
        Tutorial = 0,
        Level1,
        Level2,
        Level3
    }
    [SerializeField] public Player player;
    [SerializeField] public Level level;
    
    void Start()
    {
        AssignItemToPlayer(player);
    }

    void AssignItemToPlayer(Player player)
    {
        this.player = player; // if to change texture, etc
    }
}
