using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class GateManager : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.gameObject.CompareTag("Player"))
        {
            foreach (Transform child in this.gameObject.transform)
            {
                child.gameObject.SetActive(true);
            }
        }
    }
}
