using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LV1ButtonController : MonoBehaviour
{
    [SerializeField] public PlayerItem.Level level;
    [SerializeField] GameObject[] P1buttons;
    [SerializeField] GameObject[] P2buttons;
    [SerializeField] GameObject[] gates;
    // materials
    [HideInInspector] public Material[] PlayerMaterials;
    [HideInInspector] public Material defaultMaterial;
    
    // gates
    [SerializeField] GameObject P1gatePath;
    [SerializeField] GameObject P2gatePath;
    private Renderer[] P1gatePaths;
    private Renderer[] P2gatePaths;
    
    // player pass
    private bool P1pass;
    private bool P2pass;
    
    // level pass
    private bool currState;
    private bool prevState;

    // Audio
    private AudioManager audio;

    void Start()
    {
        ResetLevelStatus();
        
        P1gatePaths = P1gatePath.GetComponentsInChildren<Renderer>();
        P2gatePaths = P2gatePath.GetComponentsInChildren<Renderer>();

        PlayerMaterials = GameObject.Find("PlayerColor").GetComponent<MeshRenderer>().materials;
        defaultMaterial = PlayerMaterials[2];

        audio = FindObjectOfType<AudioManager>();
    }

    public void Update()
    {
        // check the player side pass status
        P1pass = CheckButtonStatus(P1buttons);
        P2pass = CheckButtonStatus(P2buttons);
        
        // update the gate materials
        UpdateGates();

        // check the level pass status
        currState = (P1pass && P2pass);
        if (currState && !prevState)
        {
            // level passed
            CheckLevelPass();
        }
        else if (!currState && prevState)
        {
            SetGateOpenStatus(false);
        }
        prevState = currState;
    }

    void SetGateOpenStatus(bool status)
    {
        foreach (GameObject gate in gates)
        {
            gate.SetActive(!status);
        }
    }

    bool CheckButtonStatus(GameObject[] buttons)
    {
        bool status = true;
        foreach (GameObject eachButton in buttons)
        {
            if (!eachButton.GetComponent<LV1Button>().status)
            {
                status = false;
            }
        }
        return status;
    }

    void UpdateGates()
    {
        UpdateGateMaterials(0, P1pass, P1gatePaths);
        UpdateGateMaterials(1, P2pass, P2gatePaths);
    }

    void UpdateGateMaterials(int playerIndex, bool status, Renderer[] objs)
    {
        if (status)
        {
            foreach (Renderer obj in objs)
            {
                obj.material = PlayerMaterials[playerIndex];
            }
        }
        else
        {
            foreach (Renderer obj in objs)
            {
                obj.material = defaultMaterial;
            }
        }
    }

    void CheckLevelPass()
    {
       
        SetGateOpenStatus(true);
        if (level == PlayerItem.Level.Level3)
        {
            // SFX
            audio.Play("levelPass");
            // GLITCH FX
            GameObject.FindWithTag("MainCamera").GetComponent<EditValues>().GlitchForSeconds(10.0f, 0.1f);
        }
        else
        {
            // SFX
            audio.Play("levelPass");
            // GLITCH FX
            GameObject.FindWithTag("MainCamera").GetComponent<EditValues>().GlitchForSeconds(0.6f, 0.5f);
        }

        if(level == PlayerItem.Level.Tutorial) {
            // Transition to Level 2 Music
            audio.Transition("menu ambiance", "level1ambiance", 10f);
        }

        if(level == PlayerItem.Level.Level1) {
            // Transition to Level 2 Music
            audio.Transition("level0and1 background", "level2 background", 1f);
            audio.Transition("level1ambiance", "level2ambiance", 10f);
        }

        if(level == PlayerItem.Level.Level2) {
            // Transition to Level 2 Music
            audio.Transition("level2 background", "level3 background", 1f);
            audio.Transition("level2ambiance", "level3ambiance", 10f);
        }

        if(level == PlayerItem.Level.Level3) {
            // Transition to Level 2 Music
            audio.Transition("level3 background", "game end background", 1f);
        }
    }

    void ResetLevelStatus()
    {
        currState = false;
        prevState = false;
        P1pass = false;
        P2pass = false;
    }
}
