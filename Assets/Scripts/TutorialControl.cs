using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialControl : MonoBehaviour
{

    /*
        Controls tutorial UI popups
        Ensures that tutorials are shown only once
        Controls popup fade in/out
    */

    // Tutorial Images: Move, Jump, Grab, Drop, Throw
    [SerializeField] private List<Image> tutorialImages;

    // shown[x,0] true when x fade in starts, shown[x,1] true when x fade out ends
    private bool[,] shown = new bool[7,2];

    void Awake()
    {
        for (int iter = 0; iter < tutorialImages.Count; iter++) tutorialImages[iter] = tutorialImages[iter].GetComponent<Image>();
    }

    // Functions to call to show tutorial
    public void moveUI() {
        StartCoroutine(FadeIn(tutorialImages[0],0));
        shown[0,0] = true;
    }

    public void jumpUI() {
        StartCoroutine(FadeIn(tutorialImages[1],1));
        shown[1,0] = true;
    }

    public void grabUI() {
        StartCoroutine(FadeIn(tutorialImages[2],2));
        shown[2,0] = true;
    }

    public void dropUI() {
        StartCoroutine(FadeIn(tutorialImages[3],3));
        shown[3,0] = true;
    }

    public void throwUI() {
        StartCoroutine(FadeIn(tutorialImages[4],4));
        shown[4,0] = true;
    }

    public void laserUI() {
        StartCoroutine(FadeIn(tutorialImages[5],5));
        shown[5,0] = true;
    }

    public void switchUI() {
        StartCoroutine(FadeIn(tutorialImages[6],6));
        shown[6,0] = true;
    }

    public bool tutorialShown(int selection) {
        return shown[selection,0];
    }

    public bool tutorialFadedOut(int selection) {
        return shown[selection,1];
    }

    IEnumerator FadeIn(Image image, int index) {
        Color currentColor = image.color;
        while(Mathf.Abs(currentColor.a - 1.0f) > 0.01f) {
            currentColor.a = Mathf.Lerp(currentColor.a, 1.0f, 2.5f * Time.deltaTime);
            image.color = currentColor;
            yield return null;
        }

        StartCoroutine(FadeOut(image, index));
    }

    IEnumerator FadeOut(Image image, int index) {
        Color currentColor = image.color;
        while(Mathf.Abs(currentColor.a) > 0.01f) {
            currentColor.a = Mathf.Lerp(currentColor.a, 0.0f, 2.5f * Time.deltaTime);
            image.color = currentColor;
            yield return null;
        }

        currentColor.a = 0.0f;
        image.color = currentColor;
        shown[index,1] = true;
    }
}
