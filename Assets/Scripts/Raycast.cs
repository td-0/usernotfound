using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cakeslice
{
    public class Raycast : MonoBehaviour
    {

        /*
            Controls raycast for grabbable objects
            Determines if player is within range to grab
        */

        //alternative grab system - Raycast

        private string grabbableTag = "PickupItem";

        private Transform _selection;

        // Tutorial
        
        private TutorialControl tutorial;

        void Start() {
        tutorial = GameObject.FindWithTag("TutorialCtrl").GetComponent<TutorialControl>();
        }

        void Update()
        {
            if (_selection != null)
            {
                // disable the object 
                var interactionScript = _selection.GetComponent<PickUp>();
                if (interactionScript != null)
                {
                    interactionScript.interactable = false;
                }
                var outlineComponent = _selection.GetComponent<Outline>();
                if (outlineComponent != null)
                {
                    outlineComponent.enabled = false;
                }
                _selection = null;
            }

            // raycast 
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            
            if (Physics.Raycast(ray, out hit))
            {
                Transform selection = hit.transform;
                
                // if grabbable and within the distance
                if (selection.CompareTag(grabbableTag) && hit.distance <= 4)
                {
                    // enable the object highlight
                    var interactionScript = selection.GetComponent<PickUp>();
                    if (interactionScript != null)
                    {
                        interactionScript.interactable = true;
                        if (!tutorial.tutorialShown(2) && tutorial.tutorialFadedOut(1)) {
                            tutorial.grabUI();
                        }
                    }
                    var outlineComponent = selection.GetComponent<Outline>();
                    if (outlineComponent != null)
                    {
                        outlineComponent.enabled = true;
                    }

                    var selectionRenderer = selection.GetComponent<Renderer>();

                    _selection = selection;
                }

            }
        }
    }
}
