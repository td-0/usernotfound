using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{

    /*
        Provides custom gravity to non-player objects in the scene using RigidBody
    */

    private float gravity;

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        // Gravity is different in single-player as it is not being updated over network
        if (Globals.singlePlayer) gravity = 500f;
        else gravity = 250f;

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        // Move object according to gravtational force
        Vector3 moveDirection = new Vector3(0.0f, 0.0f, 0.0f);

        if (this.transform.position.y <= 10) {
            moveDirection.y -= gravity;
        }
        else {
            moveDirection.y += gravity;
        }
        
        // Use rigidbody if it's an interactable object
        if (rb != null)
        {
            rb.AddForce(moveDirection * Time.deltaTime);
        }

    }
    
}
