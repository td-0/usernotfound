using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Laser : NetworkBehaviour
{ 

    /*
        Controls raycast laser system
        Raycasts and sends hit information to Target script
        Shows visible laser that generally follows raycast path
        Sends information to show laser over network
    */

    private LineRenderer laserRenderer;
    private float laserWidth = 0.1f;
    private string targetTag = "Target";

    private bool activePlayer = false;

    private bool isNonNetworkP2 = false;

    private GameObject NonNetworkP2;

    private bool wasCasting;
    
    // SFXs
    private AudioManager _audioManager;

    void Awake()
    {
        laserRenderer = GetComponent<LineRenderer>();

        Vector3[] initLaserPositions = new Vector3[ 2 ] { Vector3.zero, Vector3.zero };
        laserRenderer.SetPositions( initLaserPositions );
        laserRenderer.startWidth = laserWidth;
        laserRenderer.endWidth = laserWidth;

        NonNetworkP2 = GameObject.Find("Non-Network Player2");

        if(this.transform.name == "Non-Network Player2") isNonNetworkP2 = true;
        
        _audioManager = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
        if(isNonNetworkP2 && Globals.activePlayer == 2) {
            // This laser is attached to NNP2, determines whether it is current player
            activePlayer = true; 
        }
        else if(isLocalPlayer) {
            // This laser is attached to a spawned player
            if (!NonNetworkP2) activePlayer = true;
            else if (NonNetworkP2 && Globals.activePlayer == 1) activePlayer = true;
            else activePlayer = false;
        }
        else activePlayer = false;

        if(activePlayer) {
            if(Input.GetButton("Laser")) {
                castBeam ();
                laserRenderer.enabled = true;
                if (!wasCasting)
                {
                    // SFX start playing audio loop
                    _audioManager.Play("laser");
                    wasCasting = true;
                }
            }
            else {
                stopBeam();
                laserRenderer.enabled = false;
                wasCasting = false;
            }
        }
    }

    void castBeam()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;

        Vector3 pos;
        if(Physics.Raycast(ray, out raycastHit))
        {
            pos = raycastHit.point;

            Transform selection = raycastHit.transform;
                
                // If hits a target object
                if (selection.CompareTag(targetTag))
                {
                    if(this.transform.position.y <= 10) selection.GetComponent<Target>().hit(0);
                    else selection.GetComponent<Target>().hit(1);
                }
        }

        Vector3 startPoint = Vector3.zero;
        if(transform.position.y < 10) {
            startPoint.x = Mathf.Cos(Mathf.Deg2Rad * transform.localEulerAngles.y) * 0.5f + transform.position.x;
            startPoint.y = transform.position.y - 0.25f;
            startPoint.z = Mathf.Sin(Mathf.Deg2Rad * transform.localEulerAngles.y) * -0.5f + transform.position.z;
        }
        else {
            startPoint.x = Mathf.Cos(Mathf.Deg2Rad * transform.localEulerAngles.y) * -0.5f + transform.position.x;
            startPoint.y = transform.position.y + 0.25f;
            startPoint.z = Mathf.Sin(Mathf.Deg2Rad * transform.localEulerAngles.y) * 0.5f + transform.position.z;
        }
 
       laserRenderer.SetPosition(0, startPoint);
       laserRenderer.SetPosition(1, ray.GetPoint(50));

       if (isServer)
        {
            RpcCastBeam(startPoint, ray.GetPoint(50));
        }
        else
        {
            CmdCastBeam(startPoint, ray.GetPoint(50));
        }
    }

    void stopBeam() {
        if (isServer)
        {
            RpcStopBeam();
        }
        else
        {
            CmdStopBeam();
        }
    }

    // Show Laser on Client
    [ClientRpc]
    void RpcCastBeam(Vector3 zeroPoint, Vector3 onePoint)
    {
        laserRenderer.enabled = true;

        laserRenderer.SetPosition(0, zeroPoint);
        laserRenderer.SetPosition(1, onePoint);
    }

    // Show Laser on Server
    [Command(requiresAuthority = false)]
    void CmdCastBeam(Vector3 zeroPoint, Vector3 onePoint)
    {
        laserRenderer.enabled = true;

        laserRenderer.SetPosition(0, zeroPoint);
        laserRenderer.SetPosition(1, onePoint);
    }

    // Stop Laser on Client
    [ClientRpc]
    void RpcStopBeam()
    {
        laserRenderer.enabled = false;
    }

    // Stop Laser on Server
    [Command(requiresAuthority = false)]
    void CmdStopBeam()
    {
        laserRenderer.enabled = false;
    }
}
