using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals: object
{
    /*
        Contains all global variables for the game.
        Used to keep all global variables organized.
        These variables are local to each player and
        are not updated over the network.
    */

    // Singleplayer mode enabled
    public static bool singlePlayer = false;

    // Currently Active Player in Singleplayer Mode
    public static int activePlayer = 1;

    // Mouse Sensitivity
    public static float mouseSensitivity = 0.8f;

    // Background Music Volume
    public static float backgroundVolume = 0.5f;

    // SFX Volume
    public static float effectsVolume = 0.5f;

}