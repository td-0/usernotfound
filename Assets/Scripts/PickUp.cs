using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Mirror;

public class PickUp : NetworkBehaviour
{

    /*
        Controls mechanics for player to pick up, drop, 
        and throw objects in scene using RigidBody.
    */

    // Interactable 
    public bool interactable;

    // Networking
    private float distanceP1; // Distances from Players to Object
    private float distanceP2;

    // Players
    [HideInInspector] public GameObject playerIndicator; // Active if Player1 is currently controlled
    private GameObject player1;
    private GameObject player2;
    CharacterController characterController;
    
    // Control Object Behavior
    [HideInInspector] public Transform destination;
    private bool grabbed;
    private float throwSpeed;
    private float objSnapSpeed;
    private Rigidbody rigidBody;
    private bool grabbedThisFrame; // Used to prevent grabbing and throwing in same frame

    // SFXs
    private AudioManager _audioManager;

    // Sync Locations
    private SyncLocation sync;
    
    // Preventing going off map
    private bool colliding;

    // Tutorial
        
    private TutorialControl tutorial;

    private bool dropTutorialShown;

    private bool throwTutorialShown;

    private PlayerItem.Level level;
    

    void Start()
    {
	    playerIndicator = GameObject.Find("PlayerIndicator");
        characterController = GetComponent<CharacterController>();
        destination = GameObject.Find("Destination").transform;
        rigidBody = this.GetComponent<Rigidbody>();
        level = this.GetComponent<PlayerItem>().level;

        findPlayers();

        _audioManager = FindObjectOfType<AudioManager>();

        throwSpeed = 800;
        grabbed = false;
        grabbedThisFrame = false;

        sync = GetComponent<SyncLocation>();

        // Tutorial
        tutorial = GameObject.FindWithTag("TutorialCtrl").GetComponent<TutorialControl>();
    }

    void Update()
    {
		// Detect Players When They Spawn
        findPlayers();
		
		if (inPlayerBounds()) {
			// Reset grabbedThisFrame
            grabbedThisFrame = false;
        
			// If the object is within distance & raycasted
			if (interactable)
			{
				// left mouse click
				if (!grabbed && Input.GetMouseButtonUp(0))
				{

					grabbedThisFrame = true;
					Grab();

                    if (inLowerHalf()) sync.setLastInteracted(1);
                    else sync.setLastInteracted(2);
				}
			}
			// Secondary actions
			if (grabbed && !grabbedThisFrame)
			{
				// Update the object position
				SnapObject();
				
				if(!tutorial.tutorialShown(3) && tutorial.tutorialFadedOut(2)) {
					tutorial.dropUI();
				}
                if(!tutorial.tutorialShown(4) && level == PlayerItem.Level.Level1 && tutorial.tutorialFadedOut(2) && tutorial.tutorialFadedOut(3)) {
                    tutorial.throwUI();
                }

				if (Input.GetMouseButtonUp(0))
				{
					Release();
				}
				if (Input.GetMouseButtonUp(1))
				{
					Throw();
				}
			}
		}
    }
	
	// Ensures that players only interact with objects on their side
    private bool inPlayerBounds() {
        if (GameObject.Find("Non-Network Player2")) return true; // Single Player Mode

        return (GameObject.Find("Player1(Clone)").GetComponent<NetworkIdentity>().isLocalPlayer && inLowerHalf()) ||
        (GameObject.Find("Player2(Clone)").GetComponent<NetworkIdentity>().isLocalPlayer && !inLowerHalf());
    }

    // Determine if player is in lower gravitation field
    private bool inLowerHalf() {
        return this.transform.position.y <= 10f;
    }

    private void findPlayers() {
        player1 = GameObject.Find("Non-Network Player1");
        if (!player1) player1 = GameObject.Find("Player1(Clone)");

        player2 = GameObject.Find("Non-Network Player2");
        if (!player2) player2 = GameObject.Find("Player2(Clone)");
    }
    
    void Grab()
    {
	    // SFX
	    _audioManager.Play("grab");
	    // GLITCH FX
	    GameObject.FindWithTag("MainCamera").GetComponent<EditValues>().GlitchForSeconds(0.5f, 0.1f);
	    
        GetComponent<Gravity>().enabled = false;
        
        // reset movement vectors
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;
        
        grabbed = true;
    }

    void SnapObject()
    {
	    // snap object position to player
	    this.transform.position += (destination.position-this.transform.position) * Time.deltaTime * objSnapSpeed;
	    this.transform.rotation = destination.rotation;	
	    
	    // reset movement vectors to prevent collision forces
	    rigidBody.velocity = Vector3.zero;
	    rigidBody.angularVelocity = Vector3.zero;
    }
    
    void Release()
    {
        GetComponent<Gravity>().enabled = true;

        _audioManager.Play("release");
        
        // release the position
        this.transform.parent = null;

        grabbed = false;
    }
    void Throw()
    {
	    // SFX 
	    _audioManager.Play("throw");
	    
        // apply force
        rigidBody.velocity = destination.forward * throwSpeed * Time.deltaTime;
        
        // release object
        Release();
    }

    private void OnCollisionEnter(Collision other)
    {
	    // slow down to prevent the object from going through the wall
	    objSnapSpeed = 0.5f;
    }

    private void OnCollisionExit(Collision other)
    {
	    objSnapSpeed = 20.0f;
    }
}
