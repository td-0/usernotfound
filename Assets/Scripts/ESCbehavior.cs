using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ESCbehavior : MonoBehaviour {

    /*
        Simple class to open/close pause, endgame menus
    */

    [SerializeField] private GameObject mainMenu;

    [SerializeField] private GameObject pauseMenu;

    [SerializeField] private GameObject endGameMenu;

    [SerializeField] private GameObject pauseOptionsUI;

    void Start() {
        // Ensure that cursor is unlocked when scene loads
        Cursor.lockState = CursorLockMode.None;
    }

    void Update() {
		// Pause Menu
		if (Input.GetButtonDown("Pause") && !mainMenu.activeSelf && !endGameMenu.activeSelf) {
            if (!pauseMenu.activeSelf) {
                Cursor.lockState = CursorLockMode.None;
                pauseMenu.SetActive(true);
            }
            else {
                pauseMenu.SetActive(false);
                pauseOptionsUI.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

    public bool isPaused() {
        return pauseMenu.activeSelf;
    }

    public bool isEndGameMenu() {
        return endGameMenu.activeSelf;
    }
}