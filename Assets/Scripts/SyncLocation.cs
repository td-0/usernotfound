using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SyncLocation : NetworkBehaviour
{

    /*
        Sync's object's location information to other
        players over the network using Rpc and Cmd
    */

    private Vector3 lastPos;
    private Quaternion lastRot;

    [SyncVar]
    private int lastInteracted;

    void Start() {
        lastPos = transform.position;
        lastRot = transform.rotation;

        if (this.transform.position.y <= 10f) lastInteracted = 1;
        else lastInteracted = 2;
    }

    void Update()
    {

        // Update location data over network only when object moves
        if (isMoving()) {
            if (isServer && lastInteracted == 1)
            {
                RpcSyncPosRot(transform.position, transform.rotation);
            }
            else if (!isServer && lastInteracted == 2)
            {
                CmdSyncPosRot(transform.position, transform.rotation);
            }
        }

        lastPos = transform.position;
        lastRot = transform.rotation;
    }

    [Command(requiresAuthority = false)]
    void CmdSyncPosRot(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
    }

    [ClientRpc]
    void RpcSyncPosRot(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
    }

    // Sync lastInteracted Client to Server
    [Command(requiresAuthority = false)]
    void CmdSyncLastInteracted(int last) {
        lastInteracted = last;
    }

    public void setLastInteracted(int last) {
        lastInteracted = last;
        if (!isServer) CmdSyncLastInteracted(lastInteracted);
    }

    private bool isMoving() {
        return lastPos != transform.position || lastRot != transform.rotation;
    }
}
