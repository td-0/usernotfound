using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class PlayerMovement : NetworkBehaviour
{

    /*
        Controls directional movement and jump for spawned players
    */

    // Player and Camera Movement

    [SerializeField] private float speed;

    [SerializeField] private int playerNum;

    CharacterController characterController;

    private Camera cam;

    private Vector3 moveDirection = Vector3.zero;

    private Collider col;

    private bool jumping;

    private float gravity;

    // Mouse Movement

    private float mouseSensitivity;

    private float XMouse;

    private float YMouse;

    private ESCbehavior pause;

    private Image crosshair;

    // Tutorial

    private TutorialControl tutorial;

    private float startTime;

    private bool moveTutorialShown;

    private bool jumpTutorialShown;

    // Loading Text

    private Text loadingText;

    private float timer;

    // Audio
    private AudioManager audio;
    
    // Starting Sequence
    private bool gameStart;
    private EditValues glitchControl;
    [SerializeField] GameObject[] menues;

    // Start is called before the first frame update
    void Start()
    {
        // Make sure that NNP2 is deactivated in mutiplayer
        if (!Globals.singlePlayer && GameObject.Find("Non-Network Player2")) GameObject.Find("Non-Network Player2").SetActive(false);

        // Remove Player 2's wait menu when spawning
        if (!Globals.singlePlayer && playerNum == 2) GameObject.FindWithTag("Menu").SetActive(false);
        
        mouseSensitivity = Globals.mouseSensitivity;

        characterController = GetComponent<CharacterController>();
        col = GetComponent<Collider>();
        cam = Camera.main;
        jumping = false;

        if (Globals.singlePlayer) gravity = 3.0f;
        else gravity = 1.5f;

        if (playerNum == 1) gravity *= -1;

        pause = GameObject.FindWithTag("Esc").GetComponent<ESCbehavior>();
        tutorial = GameObject.FindWithTag("TutorialCtrl").GetComponent<TutorialControl>();

        // Loading Text
        loadingText = GameObject.FindWithTag("Loading").GetComponent<Text>();
        Color ltColor = loadingText.color;
        ltColor.a = 1.0f;
        loadingText.color = ltColor;
        timer = 0.0f;

        // Crosshair
        crosshair = GameObject.FindWithTag("Crosshair").GetComponent<Image>();

        // Set Audio Manager
        audio = FindObjectOfType<AudioManager>();

        // Start Game Background
        audio.Transition("menu beat", "loading", 4f);
        
        // GLITCH EFFECT ON SPAWN
        gameStart = false;
        //glitchControl = GameObject.FindWithTag("MainCamera").GetComponent<EditValues>();
        //StartCoroutine(DoGlitch());
        RunGlitch();
    }

    // Update is called once per frame
    void Update()
    {

        mouseSensitivity = Globals.mouseSensitivity;

        if (gameStart)
        {
            timer += Time.deltaTime;

            if (timer < 4.5f) {
            
                // Progress Loading Text
                if (timer < 4.0f) {
                    loadingText.text = "Attempting Network Reset: " + (int)(timer * 25.0f) + "% Complete";
                }
                else {
                    loadingText.gameObject.SetActive(false);
                    if (playerNum == 2) cam.transform.localEulerAngles = new Vector3(90, 0, 0);
                    if (crosshair) crosshair.enabled = true;
                    audio.Transition("loading", "level0and1 background", 0.5f);
                }
            }
        }
        else
        {
            if (loadingText) loadingText.text = "";
        }
        
        if ((timer > 4.0f) && ((isLocalPlayer && !Globals.singlePlayer) || (Globals.singlePlayer && 
                                                                            ((Globals.activePlayer == playerNum))))) {

            // Tutorials

            // Show Move Tutorial
            if (!tutorial.tutorialShown(0)) {
                tutorial.moveUI();
            }

            // Show Jump Tutorial
            if (!tutorial.tutorialShown(1) && tutorial.tutorialFadedOut(0)) {
                tutorial.jumpUI();
            }

            // Show Switch Tutorial
            if (Globals.singlePlayer && !tutorial.tutorialShown(6) && tutorial.tutorialFadedOut(1)) {
                tutorial.switchUI();
            }

            // Show Laser Tutorial
            if (!tutorial.tutorialShown(5) && transform.position.z > 10.0f) {
                tutorial.laserUI();
            }
            

            // Make Camera Follow Mouse
            XMouse += Input.GetAxis("Mouse X") * mouseSensitivity;
            YMouse -= Input.GetAxis("Mouse Y") * mouseSensitivity;

            YMouse = Mathf.Clamp(YMouse, -90f, 90f);

            if (!pause.isPaused() && !pause.isEndGameMenu()) {
                if (playerNum == 1) {
                    if (timer > 5.0f) cam.transform.localEulerAngles = new Vector3(YMouse, XMouse, 0);
                    this.transform.localEulerAngles = new Vector3(0, XMouse, 0);
                }
                else {
                    if (timer > 5.0f) cam.transform.localEulerAngles = new Vector3(YMouse * -1, XMouse * -1, 180);
                    this.transform.localEulerAngles = new Vector3(0, XMouse * -1, 180);
                }
            

                // Change Camera if not playing online
                if (Globals.singlePlayer) {
                    if (Input.GetButtonDown("CameraChangeP2")) Globals.activePlayer = 2;
                }

                cam.transform.position = this.transform.position;


                // Directional Input

                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            
                moveDirection *= speed;

                moveDirection = transform.TransformDirection(moveDirection);


                // Jumping

                if (!jumping) moveDirection.y = gravity;

                if (Input.GetButtonDown("Jump") && isGrounded()){
                    jumping = true;
                    StartCoroutine(jump(transform.position.y));
                }

                characterController.Move(moveDirection * Time.deltaTime);
            }
        }
    }

    public void RunGlitch()
    {
        glitchControl = GameObject.FindWithTag("MainCamera").GetComponent<EditValues>();
        StartCoroutine(DoGlitch());
    }

    IEnumerator DoGlitch()
    {
        glitchControl.SetGlitch(1.0f);
        
        yield return new WaitForSeconds(2.0f);
        glitchControl.SetGlitch(0f);
        yield return new WaitForSeconds(1.0f);
        StartGame();
        glitchControl.SetGlitch(0.1f);

        yield return new WaitForSeconds(5.0f);
        glitchControl.StopGlitch();
    }

    void StartGame()
    {
        // disable menues
        GameObject[] menus = GameObject.FindGameObjectsWithTag("Menu");
        foreach (var menu in menus)
        {
            menu.SetActive(false);
        }

        gameStart = true;
    }

    // Jump Function
    private IEnumerator jump(float preJumpY)
    {
        float goalPoint;

        // Stop jump if player cannot go up
        int stuckTime = 0;
        float prevY = transform.position.y;

        if (playerNum == 1) {
            goalPoint = preJumpY + 1.7f;
            while (transform.position.y < goalPoint - 0.1f) {
                transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, goalPoint, 0.2f), transform.position.z);

                if (Mathf.Round(transform.position.y) == Mathf.Round(prevY)) stuckTime++;
                if (stuckTime > 20) break;

                yield return null;
            }
            jumping = false;
        }

        else {
            goalPoint = preJumpY - 1.7f;
            while (transform.position.y > goalPoint + 0.1f) {
                transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, goalPoint, 0.2f), transform.position.z);

                if (Mathf.Round(transform.position.y) == Mathf.Round(prevY)) stuckTime++;
                if (stuckTime > 20) break;

                yield return null;
            }
            jumping = false;
        }

        
    }

    //checks if player is on the ground
    private bool isGrounded() {
        RaycastHit hit;

        if (playerNum == 1 && Physics.Raycast(transform.position, Vector3.down, out hit)) {

            Transform selection = hit.transform;

            if (selection.CompareTag("PickupItem") || hit.distance >= col.bounds.extents.y + 0.5f) return false;
            else return true;

        }
        else if (playerNum == 2 && Physics.Raycast(transform.position, Vector3.up, out hit)) {

            Transform selection = hit.transform;

            if (selection.CompareTag("PickupItem") || hit.distance >= col.bounds.extents.y + 0.5f) return false;
            else return true;
        }
        else {
            return false;
        }
    }
}
