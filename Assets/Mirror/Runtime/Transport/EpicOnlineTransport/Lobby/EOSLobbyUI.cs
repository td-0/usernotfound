﻿using Epic.OnlineServices.Lobby;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using EpicTransport;
using Mirror;

public class EOSLobbyUI : EOSLobby {
    private string lobbyName = "Singleplayer";

    private List<LobbyDetails> foundLobbies = new List<LobbyDetails>();
    private List<Attribute> lobbyData = new List<Attribute>();

    [Space(10)]
    [Header("Create Game")]
    [SerializeField] private Text nameInput;
    [SerializeField] private Text lobbyNameDisplay;

    [Header("Join Game")]
    [SerializeField] private List<GameObject> lobbySlots;
    [SerializeField] private List<Text> lobbyNames;
    [SerializeField] private List<Text> lobbyCapacities;

    // Main Menu Button Reponses

    public void singlePlayerGame()
    {
        CreateLobby(4, LobbyPermissionLevel.Publicadvertised, false,
            new AttributeData[] {new AttributeData {Key = AttributeKeys[0], Value = lobbyName},});
    }
    
    public void quitGame() {
        Application.Quit();
    }

    // Pause Menu Button Responses
    public void resume() {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Create Game
    public void createGame() {
        string gameName = nameInput.text;
        if (gameName == "") return;

        CreateLobby(4, LobbyPermissionLevel.Publicadvertised, false, new AttributeData[] { new AttributeData { Key = AttributeKeys[0], Value = gameName }, });

        // Change from Create to Start Game Menu
        lobbyNameDisplay.text = gameName;
        nameInput.transform.parent.gameObject.SetActive(false);
        lobbyNameDisplay.gameObject.SetActive(true);
    }

    // Create Lobby List
    public void drawLobbies() {
        FindLobbies();
        foreach(GameObject lobby in lobbySlots) {
            lobby.SetActive(false);
        }
        
        int iter = 0;
        foreach (LobbyDetails lobby in foundLobbies) {
            if (iter < lobbySlots.Count) {

                //get lobby name
                Attribute lobbyNameAttribute = new Attribute();
                lobby.CopyAttributeByKey(new LobbyDetailsCopyAttributeByKeyOptions { AttrKey = AttributeKeys[0] }, out lobbyNameAttribute);

                string truncatedLobbyName = lobbyNameAttribute.Data.Value.AsUtf8;

                // Do not show singleplayer games in list
                if (truncatedLobbyName == "Singleplayer") continue;

                if (truncatedLobbyName.Length > 12)
                {
                    truncatedLobbyName = truncatedLobbyName.Substring(0, 12).Trim() + "...";
                }

                int LobbyMemberCount = (int)lobby.GetMemberCount(new LobbyDetailsGetMemberCountOptions { });

                lobbySlots[iter].SetActive(true);
                lobbyNames[iter].text = truncatedLobbyName;
                lobbyCapacities[iter].text = LobbyMemberCount.ToString() + "/2";

                lobbySlots[iter].GetComponent<Button>().onClick.AddListener(() =>
                {
                    JoinLobby(lobby, AttributeKeys);
                });
            }
            iter++;
        }
    }

    //register events
    private void OnEnable() {
        //subscribe to events
        CreateLobbySucceeded += OnCreateLobbySuccess;
        JoinLobbySucceeded += OnJoinLobbySuccess;
        FindLobbiesSucceeded += OnFindLobbiesSuccess;
        LeaveLobbySucceeded += OnLeaveLobbySuccess;
    }

    //deregister events
    private void OnDisable() {
        //unsubscribe from events
        CreateLobbySucceeded -= OnCreateLobbySuccess;
        JoinLobbySucceeded -= OnJoinLobbySuccess;
        FindLobbiesSucceeded -= OnFindLobbiesSuccess;
        LeaveLobbySucceeded -= OnLeaveLobbySuccess;
    }

    //when the lobby is successfully created, start the host
    private void OnCreateLobbySuccess(List<Attribute> attributes) {
        lobbyData = attributes;

        GetComponent<NetworkManager>().StartHost();
    }

    //when the user joined the lobby successfully, set network address and connect
    private void OnJoinLobbySuccess(List<Attribute> attributes) {
        lobbyData = attributes;

        NetworkManager netManager = GetComponent<NetworkManager>();
        netManager.networkAddress = attributes.Find((x) => x.Data.Key == hostAddressKey).Data.Value.AsUtf8;
        netManager.StartClient();
    }

    //callback for FindLobbiesSucceeded
    private void OnFindLobbiesSuccess(List<LobbyDetails> lobbiesFound) {
        foundLobbies = lobbiesFound;
    }

    //when the lobby was left successfully, stop the host/client
    private void OnLeaveLobbySuccess() {
        NetworkManager netManager = GetComponent<NetworkManager>();
        netManager.StopHost();
        netManager.StopClient();

        // Reload Scene
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    private void DrawLobbyMenu() {
        //draws the lobby name
        GUILayout.Label("Name: " + lobbyData.Find((x) => x.Data.Key == AttributeKeys[0]).Data.Value.AsUtf8);

        //draws players
        for (int i = 0; i < ConnectedLobbyDetails.GetMemberCount(new LobbyDetailsGetMemberCountOptions { }); i++) {
            GUILayout.Label("Player " + i);
        }
    }
}
