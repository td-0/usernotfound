﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.Universal;

[ExecuteInEditMode]
public class EditValues : MonoBehaviour
{
    [Range(0, 1)]
    public float ChromaticGlitch;

    [Range(0, 1)]
    public float FrameGlitch;

    [Range(0, 1)]
    public float PixelGlitch;
    
    // coroutine
    private IEnumerator glitchCoroutine;

    private void Start()
    {
        StopGlitch();
    }

    void Update()
    {
        if (FastGlitchUrp.Instance == null) return;
        // FastGlitchUrp.Instance.settings.ChromaticGlitch = ChromaticGlitch;
        // FastGlitchUrp.Instance.settings.FrameGlitch = FrameGlitch;
        // FastGlitchUrp.Instance.settings.PixelGlitch = PixelGlitch;
        //FastGlitchUrp.Instance.Create();
    }

    public void StopGlitch()
    {
        SetGlitch(0);
    }

    public void SetGlitch(float value)
    {
        ChromaticGlitch = value;
        FrameGlitch = value;
        PixelGlitch = value;
        
        if (FastGlitchUrp.Instance == null) return;
        FastGlitchUrp.Instance.settings.ChromaticGlitch = ChromaticGlitch;
        FastGlitchUrp.Instance.settings.FrameGlitch = FrameGlitch;
        FastGlitchUrp.Instance.settings.PixelGlitch = PixelGlitch;
        FastGlitchUrp.Instance.Create();
    }

    public void GlitchForSeconds(float seconds, float intensity)
    {
        if (glitchCoroutine != null) StopCoroutine(glitchCoroutine);
        glitchCoroutine = GlitchCoroutine(seconds, intensity);
        StartCoroutine(glitchCoroutine);
    }

    IEnumerator GlitchCoroutine(float seconds, float intensity)
    {
        SetGlitch(intensity);
        yield return new WaitForSeconds(seconds);
        StopGlitch();
    }
}
